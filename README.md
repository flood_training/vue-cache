# vue-cache

#### 介绍
使用Vue+Vuex+axios+element-ui实现的一个ajax请求结果自动缓存。可以设置超时。如果页面刷新了，就没有缓存了。其实就是做了一个封装而已。
可以提供ajax失败后统一个一个错误提示，之后会提供自定义错误提示。随便看一下自己也可以改改。          
如果不使用缓存，那么就和普通的axios用法一样，没区别。如果使用缓存才会多一个cache对象需要传。

#### 安装教程
 **把三个文件夹下面的文件中的对应js代码贴到相应位置就OK        
views中的index.vue是一个例子。**    
不要去clone，直接run，没有模块，不能运行的。

#### 参数解读
```
cache:{
 	    key:"test",
	    timeout:30*1000,
            store:this.$store
 	}
```
key(必传)：这个就是作为缓存的时候，需要设置的一个键
timeout(可以不传):这个的单位是毫秒，超时时间，超时之后的请求就是重新请求，然后又会自动放入缓存中
store(必传)：vuex的一个store对象

#### 使用说明
注意：如果要使用缓存，那么store:this.$store这个东西必须得传      
 **1.发送get** 
```
request.get("/api/g2/getOnsInfo?name=disease_h5",{
	 params:{
		 check:122
	 }
 },{
	key:"test",
	timeout:30*1000,
	store:this.$store
	})
 .then(res=>{
 	console.log(res);
 });
```

 **2.发送post请求** 
```
request.get("/api/g2/getOnsInfo?name=disease_h5",{
	check:122
 },{
	key:"test",
	timeout:30*1000,
	store:this.$store
	})
 .then(res=>{
 	console.log(res);
 });
```

 **3.发送自定义的请求** 
```
  request.ajax({
 	url:"/api/g2/getOnsInfo?name=disease_h5",
 	method: 'get',
 	cache:{
 	    key:"test",
	    timeout:30*1000,
            store:this.$store
 	}
 }).then(res=>{
  	console.log(res);
});
```

